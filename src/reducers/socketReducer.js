
import { io } from "socket.io-client";
import { ROOT_URL } from "../config/config.default";


const socketEngine = io(`${ROOT_URL}/engine`, { transports: ['websocket'] })


export const initialState = {
    socketEngine: socketEngine
}


export const SocketReducer = (initialState, action) => {
    return {
        ...initialState
    }
}
