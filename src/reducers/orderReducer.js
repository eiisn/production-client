import { 
    ADD_ORDER_ERROR, ADD_ORDER_SUCCESS, GET_ORDERS_ERROR, 
    GET_ORDERS_SUCCESS, ORDER_TO_QUALITY_ERROR, 
    ORDER_TO_QUALITY_SUCCESS, REQUEST_ORDERS 
} from "../actions/types"


export const initialState = {
    loading: false,
    orders: [],
    newOrder: {
        product: {
            name: undefined,
            code: undefined
        },
        quantity: undefined
    },
    toQuality: {
        _id: undefined,
        product: {
            name: undefined,
            code: undefined
        },
        quantity: undefined
    },
    errorMessage: ''
}


export const orderReducer = (initialState, action) => {
    switch (action.type) {
        case REQUEST_ORDERS:
            return {
                ...initialState,
                loading: true
            }
        case GET_ORDERS_SUCCESS:
            return {
                ...initialState,
                orders: action.payload,
                loading: false
            }
        case GET_ORDERS_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            }
        case ADD_ORDER_SUCCESS:
            return {
                ...initialState,
                newOrder: action.payload,
                loading: false
            }
        case ADD_ORDER_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            }
        case ORDER_TO_QUALITY_SUCCESS:
            return {
                ...initialState,
                toQuality: true,
                loading: false
            }
        case ORDER_TO_QUALITY_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
}
