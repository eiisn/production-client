import axios from 'axios';
import { ROOT_URL } from "../config/config.default";
import { REQUEST_PRODUCTS, GET_PRODUCTS_SUCCESS, GET_PRODUCTS_ERROR, ADD_PRODUCT_SUCCESS, ADD_PRODUCT_ERROR } from './types';


export async function getProducts(dispatch) {
    
    dispatch({ type: REQUEST_PRODUCTS })

    try {
        let res = await axios.get(`${ROOT_URL}/api/product/list`)
        dispatch({ type: GET_PRODUCTS_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_PRODUCTS_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_PRODUCTS_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function addProduct(dispatch, payload) {

    dispatch({ type: REQUEST_PRODUCTS });

    console.log(payload)

    try {
        let res = await axios.post(`${ROOT_URL}/api/product/create`, payload)
        dispatch({ type: ADD_PRODUCT_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: ADD_PRODUCT_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: ADD_PRODUCT_ERROR, error: err.toString() });
        return err.toString();
    }
}