import React from 'react';

import { Route, Switch, useRouteMatch } from 'react-router';
import { useAuthState } from '../../contexts/authContext';

import List from './List';
import Register from './Register';
import Update from './Update/index';
import Profile from './Profile';
import ProfileCard from './CardProfile/ProfileCard';
import PanelButton from './CardProfile/PanelButton';
import Nav from '../Dashboard/nav';


function ProfilePage () {

    const { path } = useRouteMatch();
    const authDetails = useAuthState();

    return (
        <Switch>
            <Route exact path={path}>
                <Nav title="Profile"/>
                <div className="container">
                    <div style={{ marginTop: 48 }} className="row">
                        <ProfileCard user={authDetails.user}>
                            <PanelButton user={authDetails.user}/>
                        </ProfileCard>
                    </div>
                </div>
            </Route>
            <Route path={`${path}/user/:username`}>
                <Nav title="Profile" back={`${path}/list`}/>
                <Profile/>
            </Route>
            <Route path={`${path}/update/:username`}>
                <Nav title="Profile" back={`${path}/list`}/>
                <Update />
            </Route>
            <Route exact path={`${path}/update`}>
                <Nav title="Update profile" back={path}/>
                <Update />
            </Route>
            <Route path={`${path}/register`}>
                <Nav title="Register new user" back={path}/>
                <Register />
            </Route>
            <Route path={`${path}/list`}>
                <Nav title="User list" back={path}/>
                <List/>
            </Route>
        </Switch>
    )
}


export default ProfilePage;