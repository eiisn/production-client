import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';
import { getEngine, getLines, updateEngine } from '../../actions/engineActions';
import { 
    initialStateEngine, engineReducer, 
    lineReducer, initialStateLine 
} from '../../reducers/engineReducer';
import Success from '../Notif/Success';
import Error from '../Notif/Error';

import Preloader from '../Preloader';
import { useParams } from 'react-router';


function Update() {

    const { engineId } = useParams();

    const [stateEngine, dispatchEngine] = useReducer(engineReducer, initialStateEngine);
    const [stateLine, dispatchLine] = useReducer(lineReducer, initialStateLine)
    
    const [newIp, setNewIp] = useState('')
    const [newPort, setNewPort] = useState('')
    const [newName, setNewName] = useState('')
    const [newDescription, setNewDescription] = useState('')
    const [newLine, setNewLine] = useState('')
    const [newPosition, setNewPosition] = useState(0)

    const updateForm = () => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
        setTimeout(M.updateTextFields, 100);
    }
    
    useEffect(() => {
        getEngine(dispatchEngine, engineId);
        getLines(dispatchLine);
    }, [engineId])

    useEffect(() => {
        setNewIp(stateEngine.engine.ip);
        setNewPort(stateEngine.engine.port);
        setNewName(stateEngine.engine.name);
        setNewDescription(stateEngine.engine.description);
        setNewLine(stateEngine.engine.line);
        setNewPosition(stateEngine.engine.position);
        updateForm();
    }, [stateLine.lines, stateEngine.engine, stateEngine.updated])

    const handleSubmit = async (e) => {
        updateEngine(dispatchEngine, {
            _id: engineId,
            newIp: newIp,
            newPort: newPort,
            newName: newName,
            newDescription: newDescription,
            newLine: newLine,
            newPosition: newPosition
        })
    }

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {stateEngine.loading || stateLine.loading ? (
                    <Preloader/>
                ) :
                <div className="col s12">
                    <form>
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title">Update Engine</span>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input value={newIp} id="engine-ip" type="text" onChange={(e) => { setNewIp(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-ip">Ip</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input value={newPort} id="engine-port" type="text" onChange={(e) => { setNewPort(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-port">Port</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input value={newName} id="engine-name" type="text" onChange={(e) => { setNewName(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-name">Name</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input value={newDescription} id="engine-description" type="text" onChange={(e) => { setNewDescription(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-description">Description</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <select defaultValue="" value={newLine} onChange={(e) => { setNewLine(e.target.value) }}>
                                            <option disabled value="">Select Line</option>
                                            {stateLine.lines.map(l => {
                                                return <option key={l._id} value={l._id}>{l.name}</option>
                                            })}
                                        </select>
                                        <label>Line</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input value={newPosition} id="engine-position" type="number" onChange={(e) => { setNewPosition(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-position">Position</label>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action">
                                <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Update"/>
                            </div>
                        </div>
                    </form>
                </div>   
                }
            </div>
            {stateEngine.errorMessage && 
                <div className="row">
                    <Error title="Engine update error">
                        <p>{stateEngine.errorMessage}</p>
                    </Error>
                </div>
            }
            {stateEngine.updated &&
                <div className="row">
                    <Success title="Engine">
                        <p>Update succeed</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Update;