import { ADD_RAW_MATTER_ERROR, ADD_RAW_MATTER_SUCCESS, GET_RAW_MATTERS_ERROR, GET_RAW_MATTERS_SUCCESS, REQUEST_RAW_MATTERS } from "../actions/types"


export const initialState = {
    loading: false,
    rawMatters: [],
    newRawMatter: {
        code: undefined,
        name: undefined
    },
    errorMessage: ''
}


export const rawMatterReducer = (initialState, action) => {
    switch (action.type) {
        case REQUEST_RAW_MATTERS:
            return {
                ...initialState,
                loading: true
            }
        case GET_RAW_MATTERS_SUCCESS:
            return {
                ...initialState,
                loading: false,
                rawMatters: action.payload
            }
        case GET_RAW_MATTERS_ERROR:
            return {
                ...initialState,
                loading: false,
                errorMessage: action.error
            }
        case ADD_RAW_MATTER_SUCCESS:
            return {
                ...initialState,
                loading: false,
                newRawMatter: action.payload
            }
        case ADD_RAW_MATTER_ERROR:
            return {
                ...initialState,
                loading: false,
                errorMessage: action.error
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
}
