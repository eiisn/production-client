import { ADD_PRODUCT_ERROR, ADD_PRODUCT_SUCCESS, GET_PRODUCTS_ERROR, GET_PRODUCTS_SUCCESS, REQUEST_PRODUCTS } from "../actions/types"


export const initialState = {
    loading: false,
    products: [],
    newProduct: {
        code: undefined,
        name: undefined,
        recipe: []
    },
    errorMessage: ''
}


export const productReducer = (initialState, action) => {
    switch (action.type) {
        case REQUEST_PRODUCTS:
            return {
                ...initialState,
                loading: true
            }
        case GET_PRODUCTS_SUCCESS:
            return {
                ...initialState,
                products: action.payload,
                loading: false
            }
        case GET_PRODUCTS_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            }
        case ADD_PRODUCT_SUCCESS:
            return {
                ...initialState,
                newProduct: action.payload,
                loading: false
            }
        case ADD_PRODUCT_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
}
