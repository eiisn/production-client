import React, { useEffect, useReducer } from 'react';
import { Link } from 'react-router-dom';
import { getUserList } from '../../actions/userActions';
import { initialState, userReducer } from '../../reducers/userReducer';
import ProfileCard from './CardProfile/ProfileCard';


function List() {

    const [state, dispatch] = useReducer(userReducer, initialState)

    useEffect(() => {
        getUserList(dispatch)
    }, [])

    let listUser = state.list.map(user => {
        return (
            <ProfileCard key={user._id} user={user}>
                <div className="card-action">
                    <div className={'ui two buttons'}>
                        <Link to={`/app/profile/user/${user.username}`} className='btn blue waves-effect waves-light'>Show profile</Link>
                        <Link to={`/app/profile/update/${user.username}`} className='btn green waves-effect waves-light'>Update profile</Link>
                    </div>
                </div>
            </ProfileCard>
        )
    })

    return Boolean(listUser) ? ( 
            <div className="container">
                <div style={{ marginTop: 48 }} className="row">
                    {listUser}
                </div>
            </div> 
        ) : <div className="card-panel grey">No user found</div>
}


export default List;