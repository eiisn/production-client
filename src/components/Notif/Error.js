import React from 'react';
import Notif from './Notif';


function Err (props) {

    return (
        <Notif type="red lighten-3" title={props.title}>
            {props.children}
        </Notif>
    )
}

export default Err;