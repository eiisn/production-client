import React, { useEffect, useReducer } from 'react';
import M from 'materialize-css';
import { getRawMatters } from '../../actions/rawMatterActions';
import { initialState, rawMatterReducer } from '../../reducers/rawMatterReducer';
import Preloader from '../Preloader';


function List() {

    const [state, dispatch] = useReducer(rawMatterReducer, initialState);

    useEffect(() => {
        getRawMatters(dispatch)
    }, [])

    useEffect(() => {
        M.AutoInit();
    }, [state.rawMatters])

    return (
        state.loading ? (
            <div style={{ marginTop: 48 }} className="container center">
                <Preloader/>
            </div>
        ) : (
            Boolean(state.rawMatters.length) ? (
                <div className="container">
                    <div className="row">
                        <ul className="collapsible popout">
                            {state.rawMatters.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <div className="collapsible-header">{item.name}</div>
                                        <div className="collapsible-body">Code: {item.code}</div>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            ) : (
                <div className="container">
                    <div className="card-panel hoverable">
                        <p>No raw matter found</p>
                    </div>
                </div>
            )
        )
    )
}


export default List;