import React from 'react';
import { Link } from 'react-router-dom';


function Nav(props) {

    return (
        <nav>
            <div className="nav-wrapper white">
                <a href="#!" className="brand-logo center black-text hide-on-med-and-down">{props.title}</a>
                <a href="#!" className="brand-logo right black-text hide-on-large-only">{props.title}</a>
                {props.back &&
                    <Link to={props.back} style={{ paddingLeft: 12 }} className="black-text left"><i className="material-icons">arrow_back</i></Link>
                }
                <a href="#!" data-target="sidenav" className="sidenav-trigger black-text"><i className="material-icons">menu</i></a>
            </div>
        </nav>
    )
}

export default Nav;