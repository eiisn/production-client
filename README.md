# CheckList :
## User
- [x] Login
- [x] Role admin and supervisor can register basic
- [x] View profile
- [x] View role basic profile for admin and supervisor
- [x] Update basic profile for admin and supervisor
## Job stuff
### Product and matter
- [ ] Add product for supervisor and admin
- [ ] Add raw matter for supervisor and admin
- [ ] Update product for supervisor and admin
- [ ] Update raw matter for supervisor and admin
### Engine
- [x] View list of engine for supervisor, admin and basic
    - [ ] Add more details to engines
- [ ] Add engine for supervisor and admin
    - [x] Connection to raspberry pi with ip
- [ ] Update engine for supervisor and admin

### Orders
- [ ] Add orders list
- [ ] See orders status

### PWA
- [x] Create PWA

## Site utils
> - [Semantic UI](https://react.semantic-ui.com/)
> - [React Hook References](https://fr.reactjs.org/docs/hooks-reference.html#useeffect)
> - [Exemple useContext / useReducer 1](https://github.com/apark0720/spearmint/blob/master/src/App.jsx)
> - [Exemple useContext / useReducer 2](https://soshace.com/react-user-login-authentication-using-usecontext-and-usereducer/)
> - [Exemple useContext / useReducer 3](https://github.com/nero2009/login-auth-useContext/blob/master/src/Context/actions.js)
> - [React Hook fireship youtube](https://www.youtube.com/watch?v=TNhaISOUy6Q&list=PL0vfts4VzfNgUUEtEjxDVfh4iocVR3qIb&index=2)
