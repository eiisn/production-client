import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';
import { addProduct } from '../../actions/productActions';
import { initialState as initialStateProduct, productReducer } from '../../reducers/productReducer';
import Success from '../Notif/Success';
import Error from '../Notif/Error';
import Preloader from '../Preloader';
import { initialState as initialStateRawMatter, rawMatterReducer } from '../../reducers/rawMatterReducer';
import { getRawMatters } from '../../actions/rawMatterActions';


function Add() {

    const [stateProduct, dispatchProduct] = useReducer(productReducer, initialStateProduct);
    const [stateRawMatter, dispatchRawMatter] = useReducer(rawMatterReducer, initialStateRawMatter)

    const [code, setCode] = useState(undefined);
    const [name, setName] = useState(undefined);
    const [rawMatter, setRawMatter] = useState("");
    const [quantity, setQuantity] = useState(0);
    const [recipe, setRecipe] = useState([]);

    useEffect(() => {
        getRawMatters(dispatchRawMatter);
    }, [])

    useEffect(() => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
    }, [stateRawMatter.rawMatters])

    const handleSelectRawMatter = (e) => {
        let index = stateRawMatter.rawMatters.findIndex(item => item.code === e.target.value);
        console.log(stateRawMatter.rawMatters[index]);
        setRawMatter(stateRawMatter.rawMatters[index]);
    }

    const handleAddRawMatter = (e) => {
        setRecipe(rec => [...rec, {
            quantity: quantity,
            rawMatter: rawMatter
        }]);
    }

    const handleRemoveRawMatter = (raw) => {
        console.log("Delete: ", raw);
        setRecipe(recipe.filter(item => item.rawMatter.name !== raw));
    }

    const handleSubmit = async (e) => {
        addProduct(dispatchProduct, {
            code: code,
            name: name,
            recipe: recipe 
        });
    }

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {stateProduct.loading ? (
                    <Preloader/>
                ) :
                <div className="col s12">
                    <form>
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title">Create New Product</span>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input required id="product-name" type="text" onChange={(e) => { setName(e.target.value) }} className="validate"/>
                                        <label htmlFor="product-name">Name</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input required id="product-code" type="text" onChange={(e) => { setCode(e.target.value) }} className="validate"/>
                                        <label htmlFor="product-code">Code</label>
                                    </div>
                                </div>
                            </div>
                            <div className="card-content">
                                <span className="card-title">Add Raw Matter to Product recipe</span>
                                <div className="row">
                                    <div className="input-field col s12 m4">
                                        <select defaultValue="" onChange={handleSelectRawMatter}>
                                            <option value="" disabled>Select raw matter</option>
                                            {stateRawMatter.rawMatters.map(r => {
                                                return <option key={`${r.code}-option`} value={r.code}>{r.name}</option>
                                            })}
                                        </select>
                                        <label>Raw Matters recipe</label>
                                    </div>
                                    <div className="input-field col s4">
                                        <input id="raw-matter-quantity" type="number" onChange={(e) => { setQuantity(e.target.value) }} className="validate"/>
                                        <label htmlFor="raw-matter-quantity">Quantity</label>
                                    </div>
                                    <div className="col s8 m4">
                                        <input type="button" className="btn blue waves-effect waves-light" value="Add to Recipe" onClick={handleAddRawMatter}/>
                                    </div>
                                </div>
                            </div>
                            <div className="card-content">
                                {Boolean(recipe.length) && <span className="card-title">Recipe:</span>}
                                {recipe.map(r => {
                                    return <div key={`${r.code}-chip`} className="chip">{r.rawMatter.name} x{r.quantity}<i className="close material-icons" onClick={() => handleRemoveRawMatter(r.rawMatter.name)}>close</i></div>
                                })}
                            </div>
                            <div className="card-action">
                                <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Create"/>
                            </div>
                        </div>
                    </form>
                </div>}
            </div>
            {stateProduct.errorMessage && 
                <div className="row">
                    <Error title="Product creation error">
                        <p>{stateProduct.errorMessage}</p>
                    </Error>
                </div>
            }
            {stateProduct.newProduct.name &&
                <div className="row">
                    <Success title="Product created">
                        <p>Product: {stateProduct.newProduct.name} [{stateProduct.newProduct.code}]</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Add;