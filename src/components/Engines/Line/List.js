import React, { useEffect, useReducer } from 'react';
import M from 'materialize-css';
import { getLines } from '../../../actions/engineActions';
import { lineReducer, initialStateLine } from '../../../reducers/engineReducer';
import Preloader from '../../Preloader';


function List() {

    const [state, dispatch] = useReducer(lineReducer, initialStateLine);

    useEffect(() => {
        getLines(dispatch)
    }, [])

    useEffect(() => {
        M.AutoInit();
    }, [state.lines])

    return (
        state.loading ? (
            <div style={{ marginTop: 48 }} className="container center">
                <Preloader/>
            </div>
        ) : (
            Boolean(state.lines.length) ? (
                <div className="container">
                    <div className="row">
                        <ul className="collection">
                            {state.lines.map((item, index) => {
                                return (
                                    <li className="collection-item" key={index}>{item.name}</li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            ) : (
                <div className="container">
                    <div className="card-panel hoverable">
                        <p>No raw matter found</p>
                    </div>
                </div>
            )
        )
    )
}


export default List;