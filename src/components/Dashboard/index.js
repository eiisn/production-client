import React, { useEffect, useState } from 'react';
import M from 'materialize-css';
import jwt_decode from 'jwt-decode';

import { logout } from '../../actions/authActions';
import { useAuthDispatch, useAuthState } from '../../contexts/authContext';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

import NotFound from '../NotFound';
import Profile from '../../components/Profile';
import EnginePage from '../../components/Engines';
import OrderPage from '../Order';
import ProductPage from '../Product';
import RawMatterPage from '../RawMatter';
import Nav from './nav';
import { PRODUCTION_NAME } from '../../config/config.default';
import Info from '../Notif/Info';


function Dashboard() {

	const { path, url } = useRouteMatch();
	const history = useHistory();

	const dispatch = useAuthDispatch();
	const userDetails = useAuthState();

	const [sidenav, setSidenav] = useState(undefined);

	useEffect(() => {
		return history.listen((location) => {
			if (Boolean(userDetails.token)) {
				const decoded = jwt_decode(userDetails.token);
				const currentTime = Date.now() / 1000;
				if (decoded.exp < currentTime) {
					logout(dispatch, 'Session expired, please relogin')
				}
			}
		})
	}, [history, userDetails, dispatch])

	useEffect(() => {
		M.Sidenav.init(sidenav)
	}, [sidenav])

	useEffect(() => {
		if (userDetails.user.passwordChange) {
			history.push('/app/profile/update')
		}
	}, [history, userDetails.user.passwordChange])
	
	return (
		<>
			<header>
				<ul id="sidenav" className="sidenav sidenav-fixed" ref={(obj) => { setSidenav(obj) }}>
					<li>
						<div className="user-view">
							<Link to={`${url}`}>
								<img width={64} src="/factory.png" className="circle responsive-img" alt="production"/>
								{PRODUCTION_NAME}
							</Link>
							
						</div>
					</li>
					<li><div className="divider"/></li>
					<li><NavLink to={`${url}/engines`} activeClassName="active" className="sidenav-close">Engines</NavLink></li>
					<li><NavLink to={`${url}/orders`} activeClassName="active" className="sidenav-close">Orders</NavLink></li>
					<li><NavLink to={`${url}/products`} activeClassName="active" className="sidenav-close">Products</NavLink></li>
					<li><NavLink to={`${url}/rawmatter`} activeClassName="active" className="sidenav-close">Raw Matter</NavLink></li>
					<li><NavLink to={`${url}/profile`} activeClassName="active" className="sidenav-close">Profile</NavLink></li>
					<li><Link to='#!' onClick={() => logout(dispatch, 'Disconnected by user, see you soon !')} className="sidenav-close">Logout</Link></li>
				</ul>
			</header>
			<main>
				<Switch>
					<Route exact path={path}>
						<Nav title="Production Line"/>
						<div className="container">
							<div style={{ marginTop: 48 }} className="row">
								<Info title="Info">
									<p>Welcome on production line</p>
								</Info>
							</div>
						</div>
					</Route>
					<Route path={`${path}/engines`}>
						<EnginePage/>
					</Route>
					<Route path={`${path}/orders`}>
						<OrderPage/>
					</Route>
					<Route path={`${path}/products`}>
						<ProductPage/>
					</Route>
					<Route path={`${path}/rawmatter`}>
						<RawMatterPage/>
					</Route>
					<Route path={`${path}/profile`}>
						<Profile/>
					</Route>
					<Route path={`${path}/*`}>
						<NotFound/>
					</Route>
				</Switch>
			</main>
		</>
	)
}

export default Dashboard;