import React from 'react';


function ProfileCard (props) {

    const getImage = () => {
        switch (props.user.role) {
            case 'supervisor':
                return '/supervisor.png'
            case 'admin':
                return '/management.png'
            default:
                return '/engineer.png'
        }    
    }

    return (
        <div className="card">
            <div className="card-content">
                <ul className="collection">
                    <li className="collection-item avatar">
                        <img src={getImage()} alt="" className="circle"/>
                        <span className="title">{props.user.username}</span>
                        <span className="new badge grey" data-badge-caption="">{props.user.role}</span>
                    </li>
                </ul>
            </div>
            { props.children }
        </div>
    )
}


export default ProfileCard;
