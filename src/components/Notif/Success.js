import React from 'react';
import Notif from './Notif';


function Success (props) {

    return (
        <Notif type="green lighten-3" title={props.title}>
            {props.children}
        </Notif>
    )
}

export default Success;