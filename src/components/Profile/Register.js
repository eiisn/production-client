import React, { useEffect, useReducer, useState } from 'react';
import { useHistory } from 'react-router';
import M from 'materialize-css';

import { registerUser } from '../../actions/registerActions';
import { ADMIN, rolesAdmin, rolesSupervisor, SUPERVISOR } from '../../config/config.default';
import { useAuthState } from '../../contexts/authContext';
import { initialState, registerReducer } from '../../reducers/registerReducer';


function Register() {

    let history = useHistory();

    const [username, setUsername] = useState("");
    const [role, setRole] = useState(rolesSupervisor[0])

	const userDetails = useAuthState();
    const [state, dispatch] = useReducer(registerReducer, initialState)

    useEffect(() => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
    }, [])

    const handleSubmit = async(e) => {
        await registerUser(dispatch, {
            username: username,
            role: role
        })
    }

    let roles;
    if (![SUPERVISOR, ADMIN].includes(userDetails.user.role)) {
        history.push('/app')
    }

    if (userDetails.user.role === ADMIN) {
        roles = rolesAdmin
    } else {
        roles = rolesSupervisor
    }

    return (
        <div className="row">
            <form>
                <div className="card">
                    <div className="card-content">
                        <span className="card-title">New User</span>
                        {state.errorMessage !== null &&
                            <div className="card-panel">
                                <span className="card-title">Erreur {state.errorMessage.status}</span>
                                <p>{state.errorMessage}</p>
                            </div>
                        }
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="username" type="text" onChange={(e) => { setUsername(e.target.value) }} className="validate"/>
                                <label htmlFor="username">Username</label>
                            </div>
                            <div className="input-field col s12">
                                <select onChange={(e) => { setRole(e.target.value) }}>
                                    {roles.map((r) => {
                                        return <option key={r.key} value={r.value}>{r.text}</option>
                                    })}
                                </select>
                                <label>Role</label>
                            </div>
                        </div>
                    </div>
                    <div className="card-action">
                        <input type="button" onClick={handleSubmit} value="Register" className="btn blue"/>
                    </div>
                </div>
            </form>
            {state.username !== "" && 
                <div className="card-panel green lighten-3">
                    <span className="card-title">User added</span>
                    <ul className="collection hoverable">
                        <li className="collection-item">
                            <span className="title">Username</span>
                            <p>{state.username}</p>
                        </li>
                        <li className="collection-item">
                            <input id="newpassword" readOnly value={state.password} type="text"/>
                            <label htmlFor="newpassword">Password <i>(please note password, it will show just one time)</i></label>
                        </li>
                        <li className="collection-item">
                            <span className="title">Role</span>
                            <p>{state.role}</p>
                        </li>
                    </ul>
                </div>
            }
        </div>
    )
}

export default Register;