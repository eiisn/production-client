import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import Nav from '../Dashboard/nav';
import Add from './Add';
import List from './List';
import Update from './Update';
import { default as LineList } from './Line/List';
import { default as LineAdd } from './Line/Add';
import { default as LineUpdate } from './Line/Update';


function EnginePage () {

    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route exact path={path}>
                <Nav title="Engines"/>
                <div className="container">
                    <div className="row">
                        <div className="collection">
                            <Link className="collection-item" to={`${path}/list`}>Engine list</Link>
                            <Link className="collection-item" to={`${path}/line/list`}>Line list</Link>
                            <Link className="collection-item" to={`${path}/add`}>Add Engine</Link>
                            <Link className="collection-item" to={`${path}/line/add`}>Add New Line</Link>
                        </div>
                    </div>
                </div>
            </Route>
            <Route path={`${path}/list`}>
                <Nav title="Engines list" back={path}/>
                <List />
            </Route>
            <Route path={`${path}/add`}>
                <Nav title="Add Engine" back={path}/>
                <Add />
            </Route>
            <Route path={`${path}/update/:engineId`}>
                <Nav title="Update Engine" back={`${path}/list`}/>
                <Update />
            </Route>
            <Route path={`${path}/line/add`}>
                <Nav title="Add Line" back={path}/>
                <LineAdd />
            </Route>
            <Route path={`${path}/line/list`}>
                <Nav title="Line list" back={path}/>
                <LineList />
            </Route>
            <Route path={`${path}/line/update/:lineId`}>
                <Nav title="Update Line" back={`${path}/list`}/>
                <LineUpdate/>
            </Route>
        </Switch>
    )
}


export default EnginePage;