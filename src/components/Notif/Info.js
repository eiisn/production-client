import React from 'react';
import Notif from './Notif';


function Info (props) {

    return (
        <Notif type="blue lighten-3" title={props.title}>
            {props.children}
        </Notif>
    )
}

export default Info;