import React, { useEffect, useState } from 'react';
import M from 'materialize-css';
import { useSocket } from '../../contexts/socketContext';
import { Link } from 'react-router-dom';


function Engine(props) {
    
    const { socketEngine } = useSocket()
    const [status, setStatus] = useState(false)
    const [step, setStep] = useState("unknown")
    const [capacity, setCapacity] = useState(props.engine.capacity)
    const [quantity, setQuantity] = useState(props.engine.quantityProduced)

    useEffect(() => {
        M.AutoInit();

        socketEngine.on('connect', () => {
            setStatus(true)
            setStep('?');
            socketEngine.emit('get-status');
        })

        socketEngine.on('disconnect', () => {
            setStatus(false);
            setStep('?');
        })

        socketEngine.on('status', (res) => {
            if (res.engine === props.engine._id) {
                setStatus(res.status)
            }
        })

        socketEngine.on('step', (res) => {
            if (res.engine === props.engine._id) {
                setStep(res.step)
            }
        })

        socketEngine.on('capacity', (res) => {
            if (res.engine === props.engine._id) {
                setCapacity(res.capacity);
            }
        })

        socketEngine.on('quantity', (res) => {
            if (res.engine === props.engine._id) {
                setQuantity(res.quantity);
            }
        })
        /* console.log("Join engine: ", props.engine._id)
        socketEngine.emit('join-engine', props.engine._id) */
        socketEngine.emit('get-status');
        socketEngine.emit('get-step');

        return () => {
            socketEngine.off('connect');
            socketEngine.off('disconnect');
            socketEngine.off('status');
            socketEngine.off('step');
            socketEngine.off('capacity');
            socketEngine.off('quantity');
        }
    }, [props.engine, socketEngine])

    return (
        <li>
            <div className="collapsible-header">
                {props.engine.name}<span className={status ? 'new badge green' : 'new badge red' } data-badge-caption={status ? "Online" : "Offline"}/>
            </div>
            <div className="collapsible-body">
                <ul className="collection">
                    <li className="collection-item">Status:<span className={status ? 'new badge green' : 'new badge red' } data-badge-caption={status ? "Online" : "Offline"}/></li>
                    <li className="collection-item">Current Step:<span className="new badge grey" data-badge-caption={step}/></li>
                    <li className="collection-item">Line Position:<span className="new badge grey" data-badge-caption={props.engine.position}/></li>
                    <li className="collection-item">Capacity:<span className="new badge grey" data-badge-caption={`${capacity}/${props.engine.maxCapacity}`}/></li>
                    <li className="collection-item">Total produced:<span className="new badge grey" data-badge-caption={quantity}/></li>
                </ul>
                <div className="card-action">
                    <Link to={`/app/engines/update/${props.engine._id}`} className="btn blue waves-effect waves-light">Update</Link>
                </div>
            </div>
        </li>
    )
}


export default Engine;