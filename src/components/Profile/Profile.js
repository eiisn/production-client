import React, { useEffect, useReducer } from 'react';
import { useParams } from 'react-router-dom';
import { getUser } from '../../actions/userActions';
import { useAuthState } from '../../contexts/authContext';
import { initialState, userReducer } from '../../reducers/userReducer';
import ProfileCard from './CardProfile/ProfileCard';


function Profile() {

    const { username } = useParams();
    const userDetails = useAuthState();
    
    if (username === userDetails.user.username) {
        initialState.user.username = userDetails.user.username;
        initialState.user.role = userDetails.user.role;
        initialState.user.userId = userDetails.user._id;
    }

    const [state, dispatch] = useReducer(userReducer, initialState);

    useEffect(() => {
        if (username !== userDetails.user.username) {
            getUser(dispatch, { username: username })
        }
    }, [userDetails.user, username])

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                <ProfileCard user={state.user} />
            </div>
        </div>
    )
}

export default Profile;