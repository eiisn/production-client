import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';

import { updateUser } from '../../../actions/userActions';
import { useAuthDispatch, useAuthState } from '../../../contexts/authContext';
import { initialState, userReducer } from '../../../reducers/userReducer';
import { rolesAdmin, rolesSupervisor } from '../../../config/config.default';
import { logout } from '../../../actions/authActions';


export default function Form({ user, changeRole }) {

    const authDispatch = useAuthDispatch();
    const userDetails = useAuthState();

    const [state, dispatch] = useReducer(userReducer, initialState);
    const [newRole, setNewRole] = useState(user.role);
    const [newPassword, setNewPassword] = useState(undefined);
    const [confirmPassword, setConfirmPassword] = useState(undefined);
    const [passwordError, setPasswordError] = useState(false);
    const [successUpdate, setSuccessUpdate] = useState(false);
    const [roles, setRoles] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            let selectElements = document.querySelectorAll('select');
            M.FormSelect.init(selectElements, {
                dropdownOptions: {
                    coverTrigger: false
                }
            });
            M.updateTextFields()
        }, 100)
    }, [user.role])

    useEffect(() => {
        if (user.role === 'admin') {
            setRoles(rolesAdmin)
        } else {
            setRoles(rolesSupervisor)
        }
    }, [user.role])

    const handleUpdate = async (e) => {
        if (newPassword !== confirmPassword) {
            setPasswordError(true)
        } else {
            let response = await updateUser(dispatch, {
                username: user.username,
                newPassword: newPassword,
                newRole: newRole
            })

            if (response) setSuccessUpdate(true)

            if (Boolean(newPassword) && Boolean(confirmPassword) && userDetails.user.username === user.username) {
                logout(authDispatch, "Password changed, please reconnect with new password")
            }
        }
    }

    return (
        <form>
            <div className="card">
                <div className="card-content">
                    <span className="card-title">Update Profile {state.user.username}</span>
                    {passwordError &&
                        <div className="card-panel red">
                            <p>New password and confirm password are different</p>
                        </div>
                    }
                    {Boolean(state.errorMessage) &&
                        <div className="card-panel red">
                            {state.errorMessage}
                        </div>
                    }
                    {successUpdate && !passwordError &&
                        <div className="card-panel green">
                            <p>Profile updated</p>
                        </div>
                    }
                    <div className="row">
                        <div className="input-field col s12">
                            <input className="validate" disabled id="username" type="text" readOnly value={user.username ? user.username : ""}/>
                            <label htmlFor="username">Username</label>
                        </div>
                        <div className="input-field col s12">
                            <select disabled={changeRole} onChange={(e) => { setNewRole(e.target.value) }}>
                                <option key="empty" value="" disabled>Select a Role</option>
                                {roles.map((r) => {
                                    return <option key={r.key} value={r.value} selected={r.value === user.role}>{r.text}</option>
                                })}
                            </select>
                            <label>Role</label>
                        </div>
                        <div className="input-field col s12">
                            <input className="validate" id="new-password" type="password" onChange={(e) => setNewPassword(e.target.value)}/>
                            <label htmlFor="new-password">New password</label>
                        </div>
                        <div className="input-field col s12">
                            <input className="validate" id="confirm-password" type="password" onChange={(e) => setConfirmPassword(e.target.value)}/>
                            <label htmlFor="confirm-password">Confirm password</label>
                        </div>
                    </div>
                </div>
                <div className="card-action">
                    <input type="button" className="btn green waves-effect waves-light" onClick={handleUpdate} value="Update"/>
                </div>
            </div>
        </form>
    )
}