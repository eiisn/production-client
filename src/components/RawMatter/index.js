import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import Nav from '../Dashboard/nav';
import Add from './Add';
import List from './List';


function RawMatterPage () {

    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route exact path={path}>
                <Nav title="Raw Matter Stock"/>
                <div className="container">
                    <div className="row">
                        <div className="collection">
                            <Link className="collection-item" to={`${path}/list`}>Raw Matter list</Link>
                            <Link className="collection-item" to={`${path}/add`}>Add Raw Matter</Link>
                        </div>
                    </div>
                </div>
            </Route>
            <Route path={`${path}/list`}>
                <Nav title="Raw Matter Stock" back={path}/>
                <List />
            </Route>
            <Route path={`${path}/add`}>
                <Nav title="Raw Matter Stock" back={path}/>
                <Add />
            </Route>
        </Switch>
    )
}


export default RawMatterPage;