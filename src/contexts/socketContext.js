import React, { useReducer } from 'react';
import { initialState, SocketReducer } from '../reducers/socketReducer';

const SocketContext = React.createContext();
const SocketDispatchContext = React.createContext();

export function useSocket() {

    const context = React.useContext(SocketContext)

    if (context === undefined) {
        throw new Error('useSocket must be used within a SocketProvider')
    }

    return context;
}

export function useSocketDispatch() {

    const context = React.useContext();

    if (context === undefined) {
        throw new Error('useSocketDispatch must be used within a SocketProvider')
    }

    return context;
}

export const SocketProvider = ({ children }) => {

    const [socket, dispatch] = useReducer(SocketReducer, initialState);

    return (
        <SocketContext.Provider value={socket}>
            <SocketDispatchContext.Provider value={dispatch}>
                {children}
            </SocketDispatchContext.Provider>
        </SocketContext.Provider>
    )
}