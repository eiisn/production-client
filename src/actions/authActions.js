import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { ROOT_URL } from '../config/config.default';
import { LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT, REQUEST_LOGIN } from './types';


export async function loginUser(dispatch, loginPayload) {

    dispatch({ type: REQUEST_LOGIN });

    axios.post(`${ROOT_URL}/api/authentication/login`, loginPayload).then((res) => {

        const { token } = res.data;
        const decoded = jwt_decode(token);
        const data = {
            user: decoded,
            token: token
        }
        
        localStorage.setItem('token', token);
        setAuthToken(token);
        dispatch({ type: LOGIN_SUCCESS, payload: data});
        return data;
    }).catch((err) => {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: LOGIN_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: LOGIN_ERROR, error: err.toString() });
        return err.toString();
    })
}


export async function logout(dispatch, message) {
    localStorage.removeItem('token');
	localStorage.removeItem('currentUser');
	dispatch({ type: LOGOUT, reason: message });
    setAuthToken(false);
}


export const setAuthToken = token => {
    if (token) {
        axios.defaults.headers.common["Authorization"] = token;
    } else {
        delete axios.defaults.headers.common["Authorization"];
    }
};
