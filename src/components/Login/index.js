import React, { useEffect, useState } from 'react';

import { loginUser } from '../../actions/authActions';
import { useHistory } from 'react-router';
import { useAuthState, useAuthDispatch } from '../../contexts/authContext';
import Info from '../Notif/Info';
import Err from '../Notif/Error';
import { RESET } from '../../actions/types';


function Login() {

	let history = useHistory();

	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const dispatch = useAuthDispatch();
	const userDetails = useAuthState();

	useEffect(() => {
		if (userDetails.user !== null) {
			history.push('/app')
		} else {
			dispatch({ type: RESET });
		}
	}, [userDetails.user, history, dispatch])

	const handleLogin = async (e) => {
		e.preventDefault();
		loginUser(dispatch, {
			username: username, 
			password: password
		}).then((response) => {
			if (response.user !== null || response.user !== undefined) {
				history.push('/app');
			}
		}).catch((err) => {
			console.log(err)
		});
	};

	return (
		<div className="container">
			<div style={{ paddingTop: 48 }} className="row">
				<form>
					<div className="card hoverable">
						<div className="card-content">
							<span className="card-title">Connexion to Production Line</span>
							{Boolean(userDetails.errorMessage) &&
								<Err title="Erreur">
									<p>{userDetails.errorMessage}</p>
								</Err>
							}
							{Boolean(userDetails.reason) &&
								<Info title="Disconnected">
									{userDetails.reason}
								</Info>
							}
							<div className="row">
								<div className="input-field col s12">
									<input value={username} id="username" type="text" className="validate" onChange={(e) => setUsername(e.target.value)}/>
									<label htmlFor="username">Username</label>
								</div>
								<div className="input-field col s12">
									<input value={password} id="password" type="password" className="validate" onChange={(e) => setPassword(e.target.value)}/>
									<label htmlFor="username">Password</label>
								</div>
							</div>
						</div>
						<div className="card-action">
							<input onClick={handleLogin} type="submit" value="Login" className="btn blue"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	);
}

export default Login;