import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import Nav from '../Dashboard/nav';
import Add from './Add';
import List from './List';


function OrderPage () {

    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route exact path={path}>
                <Nav title="Orders"/>
                <div className="container">
                    <div className="row">
                        <div className="collection">
                            <Link className="collection-item" to={`${path}/list`}>Order list</Link>
                            <Link className="collection-item" to={`${path}/add`}>Create Order</Link>
                        </div>
                    </div>
                </div>
            </Route>
            <Route path={`${path}/list`}>
                <Nav title="Orders list" back={path}/>
                <List />
            </Route>
            <Route path={`${path}/add`}>
                <Nav title="Create Order" back={path}/>
                <Add />
            </Route>
        </Switch>
    )
}


export default OrderPage;