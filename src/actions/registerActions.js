import axios from 'axios';
import { ROOT_URL } from '../config/config.default';
import { REGISTER_ERROR, REGISTER_SUCCESS, REQUEST_REGISTER } from './types';


export async function registerUser(dispatch, registerPayload) {

    dispatch({ type: REQUEST_REGISTER})

    axios.post(`${ROOT_URL}/api/user/register`, registerPayload).then((res) => {
        dispatch({ type: REGISTER_SUCCESS, payload: res.data})
        return res.data;
    }).catch((err) => {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: REGISTER_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: REGISTER_ERROR, error: err.toString() });
        return err.toString();
    })
}
