
export const ROOT_URL = process.env.NODE_ENV === 'production' 
    ? "https://api.bellevie-production-creme.nice.cloud-save.space" 
    : "http://127.0.0.1:3030"

export const rolesSupervisor = [
    { key: "basic", value: "basic", text: "Basic"},
    { key: "supervisor", value: "supervisor", text: "Supervisor"},
]

export const rolesAdmin = [
    { key: "basic", value: "basic", text: "Basic"},
    { key: "supervisor", value: "supervisor", text: "Supervisor"},
    { key: "admin", value: "admin", text: "Administrator"},
]

export const SUPERVISOR = 'supervisor';
export const ADMIN = 'admin';
export const BASIC = 'basic';

export const PRODUCTION_NAME = 'Production Line'