import { 
    ADD_ENGINE_ERROR, ADD_ENGINE_SUCCESS, ADD_LINE_ERROR, ADD_LINE_SUCCESS, 
    GET_ENGINES_ERROR, GET_ENGINES_SUCCESS, GET_ENGINE_ERROR, GET_ENGINE_SUCCESS, 
    GET_LINES_ERROR, GET_LINES_SUCCESS, GET_LINE_ERROR, GET_LINE_SUCCESS, REQUEST_ENGINES, 
    REQUEST_LINE, REQUEST_LINES, UPDATE_ENGINE_ERROR, UPDATE_ENGINE_SUCCESS, UPDATE_LINE_ERROR, UPDATE_LINE_SUCCESS 
} from "../actions/types";


export const initialStateEngine = {
    loading: false,
    updated: false,
    engine: {
        ip: undefined,
        port: undefined,
        name: undefined,
        description: undefined,
        position: undefined,
        line: undefined
    },
    engines: [],
    newEngine: {
        ip: undefined,
        port: undefined,
        name: undefined,
        description: undefined,
        position: undefined,
        line: undefined
    },
    errorMessage: ''
}


export const initialStateLine = {
    loading: false,
    updated: false,
    lines: [],
    line: {
        name: undefined,
        description: undefined,
        usage: undefined
    },
    newLine: {
        name: undefined,
        description: undefined,
        usage: undefined
    },
    errorMessage: ''
}


export const engineReducer = (initialStateEngine, action) => {
    switch (action.type) {
        case REQUEST_ENGINES:
            return {
                ...initialStateEngine,
                loading: true,
                updated: false
            }
        case GET_ENGINES_SUCCESS:
            return {
                ...initialStateEngine,
                engines: action.payload,
                loading: false
            }
        case GET_ENGINES_ERROR:
            return {
                ...initialStateEngine,
                errorMessage: action.error,
                loading: false
            }
        case GET_ENGINE_SUCCESS:
            return {
                ...initialStateEngine,
                engine: action.payload,
                loading: false
            }
        case GET_ENGINE_ERROR:
            return {
                ...initialStateEngine,
                errorMessage: action.error,
                loading: false
            }
        case ADD_ENGINE_SUCCESS:
            return {
                ...initialStateEngine,
                newEngine: action.payload,
                loading: false
            }
        case ADD_ENGINE_ERROR:
            return {
                ...initialStateEngine,
                errorMessage: action.error,
                loading: false
            }
        case UPDATE_ENGINE_SUCCESS:
            return {
                ...initialStateEngine,
                engine: action.payload,
                updated: true,
                loading: false
            }
        case UPDATE_ENGINE_ERROR:
            return {
                ...initialStateEngine,
                errorMessage: action.error,
                loading: false
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`);
    }
}


export const lineReducer = (initialStateLine, action) => {
    switch (action.type) {
        case REQUEST_LINE:
            return {
                ...initialStateLine,
                loading: true
            }
        case GET_LINE_SUCCESS:
            return {
                ...initialStateLine,
                line: action.payload,
                loading: false
            }
        case GET_LINE_ERROR:
            return {
                ...initialStateLine,
                errorMessage: action.error,
                loading: false
            }
        case REQUEST_LINES:
            return {
                ...initialStateLine,
                loading: true
            }
        case GET_LINES_SUCCESS:
            return {
                ...initialStateLine,
                lines: action.payload,
                loading: false
            }
        case GET_LINES_ERROR:
            return {
                ...initialStateLine,
                errorMessage: action.error,
                loading: false
            }
        case UPDATE_LINE_SUCCESS:
            return {
                ...initialStateLine,
                line: action.payload,
                updated: true,
                loading: false
            }
        case UPDATE_LINE_ERROR:
            return {
                ...initialStateLine,
                errorMessage: action.error,
                loading: false
            }
        case ADD_LINE_SUCCESS:
            return {
                ...initialStateLine,
                newLine: action.payload,
                loading: false
            }
        case ADD_LINE_ERROR:
            return {
                ...initialStateLine,
                errorMessage: action.error,
                loading: false
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`)
    }
}