import axios from 'axios';
import { ROOT_URL } from "../config/config.default";
import { REQUEST_ORDERS, GET_ORDERS_SUCCESS, GET_ORDERS_ERROR, ADD_ORDER_SUCCESS, ADD_ORDER_ERROR, ORDER_TO_QUALITY_SUCCESS, ORDER_TO_QUALITY_ERROR } from './types';


export async function getOrders(dispatch) {
    
    dispatch({ type: REQUEST_ORDERS })

    try {
        let res = await axios.get(`${ROOT_URL}/api/order/list`)
        dispatch({ type: GET_ORDERS_SUCCESS, payload: JSON.parse(res.data) });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_ORDERS_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_ORDERS_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function addOrder(dispatch, payload) {

    dispatch({ type: REQUEST_ORDERS });

    try {
        let res = await axios.post(`${ROOT_URL}/api/order/create`, payload)
        dispatch({ type: ADD_ORDER_SUCCESS, payload: res.data });
        return res.data;
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: ADD_ORDER_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: ADD_ORDER_ERROR, error: err.toString() });
        return err.toString();
    }
}

export async function sendToQuality(dispatch, payload) {

    dispatch({ type: REQUEST_ORDERS });

    try {
        let res = await axios.post(`${ROOT_URL}/api/order/quality`, payload);
        dispatch({ type: ORDER_TO_QUALITY_SUCCESS, payload: res.data });
        return res.data;
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: ORDER_TO_QUALITY_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: ORDER_TO_QUALITY_ERROR, error: err.toString() });
        return err.toString();
    }
}