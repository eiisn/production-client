import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';
import { addOrder } from '../../actions/orderActions';
import { initialState as initialStateOrder, orderReducer } from '../../reducers/orderReducer';
import { initialState as initialStateProduct, productReducer } from '../../reducers/productReducer';
import Success from '../Notif/Success';
import Error from '../Notif/Error';
import Preloader from '../Preloader';
import { getProducts } from '../../actions/productActions';


function Add() {

    const [stateOrder, dispatchOrder] = useReducer(orderReducer, initialStateOrder);
    const [stateProduct, dispatchProduct] = useReducer(productReducer, initialStateProduct)

    const [product, setProduct] = useState(undefined)
    const [quantity, setQuantity] = useState(undefined)

    const handleSubmit = async (e) => {
        addOrder(dispatchOrder, {
            productCode: product,
            quantity: quantity 
        })
    }

    useEffect(() => {
        getProducts(dispatchProduct);
    }, [])

    useEffect(() => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
    }, [stateProduct.products, stateOrder.newOrder])

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {stateOrder.loading || stateProduct.loading ? (
                    <Preloader/>
                ) :
                <div className="col s12">
                    <form>
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title">Create New Order</span>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <select required defaultValue="" onChange={(e) => { setProduct(e.target.value) }}>
                                            <option value="" disabled>Select Product to produce</option>
                                            {stateProduct.products.map(p => {
                                                return <option key={p._id} value={p.code}>{p.name}</option>
                                            })}
                                        </select>
                                        <label>Product</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input required id="order-quantity" type="number" onChange={(e) => { setQuantity(e.target.value) }} className="validate"/>
                                        <label htmlFor="order-quantity">Quantity</label>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action">
                                <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Create"/>
                            </div>
                        </div>
                    </form>
                </div>}
            </div>
            {stateOrder.errorMessage && 
                <div className="row">
                    <Error title="Order creation error">
                        <p>{stateOrder.errorMessage}</p>
                    </Error>
                </div>
            }
            {stateOrder.newOrder.product.name &&
                <div className="row">
                    <Success title="Order created">
                        <p>Order: {stateOrder.newOrder.product.name} x{stateOrder.newOrder.quantity}</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Add;