import { useRouteMatch } from "react-router";
import { Link } from "react-router-dom";


function PanelButton ({ user }) {

    const { url } = useRouteMatch();

    return (
        <div className="card-action">
            <Link style={{ marginRight: 12 }} to={`${url}/update`} className='btn green waves-effect waves-light'>
                Update Profile
            </Link>
            {user.role !== 'basic' &&
                <>
                    <Link style={{ marginRight: 12 }} to={`${url}/list`} className='btn orange waves-effect waves-light'>
                        List Profile
                    </Link>
                    <Link style={{ marginRight: 12 }} to={`${url}/register`} className='btn purple waves-effect waves-light'>
                        Register Profile
                    </Link>
                </>
            }
        </div>
    )
}


export default PanelButton;