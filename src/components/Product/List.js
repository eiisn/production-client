import React, { useEffect, useReducer } from 'react';
import M from 'materialize-css';
import { getProducts } from '../../actions/productActions';
import { initialState, productReducer } from '../../reducers/productReducer';
import Preloader from '../Preloader';


function List() {

    const [state, dispatch] = useReducer(productReducer, initialState);

    useEffect(() => {
        getProducts(dispatch)
    }, [])

    useEffect(() => {
        let elem = document.querySelector('.collapsible.expandable');
        M.Collapsible.init(elem, {
          accordion: false
        });
    }, [state.products])

    return (
        state.loading ? (
            <div style={{ marginTop: 48 }} className="container center">
                <Preloader/>
            </div>
        ) : (
            Boolean(state.products.length) ? (
                <div className="container">
                    <div className="row">
                        <ul className="collapsible expandable">
                            {state.products.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <div className="collapsible-header">
                                            {item.name}
                                        </div>
                                        <div className="collapsible-body">
                                            <ul className="collection">
                                                <li className="collection-item">
                                                    Code: {item.code}
                                                </li>
                                                <li className="collection-item">
                                                    <p>Recipe: </p>
                                                    {item.recipe.map(r => {
                                                        return <div key={`${r.code}-chip`} className="chip">{r.rawMatter.name} x{r.quantity}</div>
                                                    })}
                                                </li>
                                            </ul>    
                                        </div>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            ) : (
                <div className="container">
                    <div className="card-panel hoverable">
                        <p>No Products found</p>
                    </div>
                </div>
            )
        )
    )
}


export default List;