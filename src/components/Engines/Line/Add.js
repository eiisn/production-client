import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';
import { addLine } from '../../../actions/engineActions';
import { initialStateLine, lineReducer } from '../../../reducers/engineReducer';
import Success from '../../Notif/Success';
import Error from '../../Notif/Error';

import Preloader from '../../Preloader';


function Add() {

    const [state, dispatch] = useReducer(lineReducer, initialStateLine);
    const [name, setName] = useState(undefined);
    const [description, setDescription] = useState(undefined);
    const [usage, setUsage] = useState(undefined);

    useEffect(() => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
    }, [])

    const handleSubmit = async (e) => {
        addLine(dispatch, {
            name: name,
            description: description,
            usage: usage
        })
    }

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {state.loading ? (
                    <Preloader/>
                ) :
                <div className="col s12">
                    <form>
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title">Create New Line</span>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id="line-name" type="text" onChange={(e) => { setName(e.target.value) }} className="validate"/>
                                        <label htmlFor="line-name">Name</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input id="line-description" type="text" onChange={(e) => { setDescription(e.target.value) }} className="validate"/>
                                        <label htmlFor="line-description">Description</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <select onChange={(e) => { setUsage(e.target.value) }}>
                                            <option key="production" value="production">Production</option>
                                            <option key="stock" value="stock">Stock</option>
                                        </select>
                                        <label>Utilisation</label>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action">
                                <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Create"/>
                            </div>
                        </div>
                    </form>
                </div>   
                }
            </div>
            {state.errorMessage && 
                <div className="row">
                    <Error title="Line creation error">
                        <p>{state.errorMessage}</p>
                    </Error>
                </div>
            }
            {state.newLine.name &&
                <div className="row">
                    <Success title="Line created">
                        <p>Line: {state.newLine.name}</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Add;