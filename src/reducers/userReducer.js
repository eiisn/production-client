import { 
    REQUEST_LIST_OF_USER, GET_LIST_OF_USER_ERROR, GET_LIST_OF_USER_SUCCESS,
    REQUEST_GET_USER, GET_USER_ERROR, GET_USER_SUCCESS,
    REQUEST_UPDATE_USER, UPDATE_USER_ERROR, UPDATE_USER_SUCCESS, RESET
} from "../actions/types";

export const initialState = {
    user: {
        userId: undefined,
        username: undefined,
        role: undefined
    },
    list: [],
    loading: false,
    errorMessage: ''
}

export const userReducer = (initialState, action) => {
    switch (action.type) {
        case RESET:
            return {
                ...initialState
            }
        case REQUEST_GET_USER:
            return {
                ...initialState,
                loading: true
            };
        case GET_USER_SUCCESS:
            return {
                ...initialState,
                user: action.payload,
                loading: false
            }
        case GET_USER_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false,
            }
        case REQUEST_LIST_OF_USER:
            return {
                ...initialState,
                loading: true
            };
        case GET_LIST_OF_USER_SUCCESS:
            return {
                ...initialState,
                list: action.payload,
                loading: false,
            }
        case GET_LIST_OF_USER_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false,
            }
        case REQUEST_UPDATE_USER:
            return {
                ...initialState,
                loading: true
            }
        case UPDATE_USER_SUCCESS:
            return {
                ...initialState,
                user: action.payload,
                loading: false
            }
        case UPDATE_USER_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`);
    }
}