import axios from 'axios';
import { ROOT_URL } from "../config/config.default";
import { REQUEST_RAW_MATTERS, GET_RAW_MATTERS_SUCCESS, GET_RAW_MATTERS_ERROR, ADD_RAW_MATTER_SUCCESS, ADD_RAW_MATTER_ERROR } from './types';


export async function getRawMatters(dispatch) {
    
    dispatch({ type: REQUEST_RAW_MATTERS })

    try {
        let res = await axios.get(`${ROOT_URL}/api/raw-matter/list`);
        dispatch({ type: GET_RAW_MATTERS_SUCCESS, payload: res.data })
        console.log(res.data);
        return res.data;
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_RAW_MATTERS_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_RAW_MATTERS_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function addRawMatter(dispatch, payload) {

    dispatch({ type: REQUEST_RAW_MATTERS });

    try {
        let res = await axios.post(`${ROOT_URL}/api/raw-matter/create`, payload)
        dispatch({ type: ADD_RAW_MATTER_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: ADD_RAW_MATTER_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: ADD_RAW_MATTER_ERROR, error: err.toString() });
        return err.toString();
    }
}