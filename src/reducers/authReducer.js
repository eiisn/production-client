import jwt_decode from 'jwt-decode';
import { LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT, REQUEST_LOGIN, RESET } from '../actions/types';

let token = localStorage.getItem('token');
let user = localStorage.getItem('token') ? jwt_decode(localStorage.getItem('token')) : null;


export const initialState = {
	user: null || user,
	token: null || token,
	loading: false,
	errorMessage: null,
	reason: ''
};

export const AuthReducer = (initialState, action) => {
	switch (action.type) {
		case RESET:
			return {
				...initialState
			}
		case REQUEST_LOGIN:
			return {
				...initialState,
				loading: true,
				reason: ''
			};
		case LOGIN_SUCCESS:
			return {
				...initialState,
				user: action.payload.user,
				token: action.payload.token,
				loading: false,
			};
		case LOGOUT:
			return {
				...initialState,
				user: null,
				token: null,
				loading: false,
				errorMessage: null,
				reason: action.reason
			};
		case LOGIN_ERROR:
			return {
				...initialState,
				loading: false,
				errorMessage: action.error,
			};
		default:
			throw new Error(`Unhandled action type: ${action.type}`);
	}
};