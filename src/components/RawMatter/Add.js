import React, { useReducer, useState } from 'react';
import { addRawMatter } from '../../actions/rawMatterActions';
import { initialState, rawMatterReducer, } from '../../reducers/rawMatterReducer';
import Success from '../Notif/Success';
import Error from '../Notif/Error';
import Preloader from '../Preloader';


function Add() {

    const [stateRawMatter, dispatchRawMatter] = useReducer(rawMatterReducer, initialState);
    
    const [name, setName] = useState(undefined)
    const [code, setCode] = useState(undefined)
    
    const handleSubmit = async (e) => {
        addRawMatter(dispatchRawMatter, {
            name: name,
            code: code
        })
    }

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {stateRawMatter.loading ? (
                    <Preloader/>
                ) : (
                    <div className="col s12">
                        <form>
                            <div className="card">
                                <div className="card-content">
                                    <span className="card-title">Create New RawMatter</span>
                                    <div className="row">
                                        <div className="input-field col s12">
                                            <input required id="rawMatter-name" type="text" onChange={(e) => { setName(e.target.value) }} className="validate"/>
                                            <label htmlFor="rawMatter-name">Name</label>
                                        </div>
                                        <div className="input-field col s12">
                                            <input required id="rawMatter-code" type="text" onChange={(e) => { setCode(e.target.value) }} className="validate"/>
                                            <label htmlFor="rawMatter-code">Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-action">
                                    <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Create"/>
                                </div>
                            </div>
                        </form>
                    </div>
                )}
            </div>
            {stateRawMatter.errorMessage && 
                <div className="row">
                    <Error title="RawMatter creation error">
                        <p>{stateRawMatter.errorMessage}</p>
                    </Error>
                </div>
            }
            {stateRawMatter.newRawMatter.name &&
                <div className="row">
                    <Success title="RawMatter created">
                        <p>RawMatter: {stateRawMatter.newRawMatter.name}</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Add;