import { REGISTER_ERROR, REGISTER_SUCCESS, REQUEST_REGISTER } from "../actions/types"

export const initialState = {
    username: "",
    password: "",
    role: "",
    loading: false,
    errorMessage: null
}

export const registerReducer = (initialState, action) => {
    switch (action.type) {
        case REQUEST_REGISTER:
            return {
                ...initialState,
                loading: true
            };
        case REGISTER_SUCCESS:
            return {
                ...initialState,
                username: action.payload.username,
                password: action.payload.password,
                role: action.payload.role,
                loading: false
            };
        case REGISTER_ERROR:
            return {
                ...initialState,
                errorMessage: action.error,
                loading: false
            };
        default:
            throw new Error(`Unhandled action type: ${action.type}`);
    }
};