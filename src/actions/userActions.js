import axios from 'axios';
import { ROOT_URL } from "../config/config.default";
import { GET_LIST_OF_USER_ERROR, GET_LIST_OF_USER_SUCCESS, GET_USER_ERROR, GET_USER_SUCCESS, REQUEST_GET_USER, REQUEST_LIST_OF_USER, REQUEST_UPDATE_USER, UPDATE_USER_ERROR, UPDATE_USER_SUCCESS } from "./types";


export async function getUser(dispatch, payload) {
    
    dispatch({ type: REQUEST_GET_USER })
    
    const { username } = payload;

    try {
        const res = await axios.get(`${ROOT_URL}/api/user/profile/${username}`);
        dispatch({ type: GET_USER_SUCCESS, payload: res.data });
        return res.data;
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_USER_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_USER_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function updateUser(dispatch, payload) {

    dispatch({ type: REQUEST_UPDATE_USER })

    try {
        let res = await axios.post(`${ROOT_URL}/api/user/update`, payload);
        dispatch({ type: UPDATE_USER_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: UPDATE_USER_ERROR, error: err.response.data.message });
                return;
            }
        }
        dispatch({ type: UPDATE_USER_ERROR, error: err.toString() });
        return;
    }
}

export async function getUserList(dispatch) {
    
    dispatch({ type: REQUEST_LIST_OF_USER })

    try {
        const res = await axios.get(`${ROOT_URL}/api/user/list`);
        dispatch({ type: GET_LIST_OF_USER_SUCCESS, payload: res.data })
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_LIST_OF_USER_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_LIST_OF_USER_ERROR, error: err.toString() });
        return err.toString();
    }   
}
