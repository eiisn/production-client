import React, { useEffect, useReducer } from 'react';
import M from 'materialize-css';
import { getOrders, sendToQuality } from '../../actions/orderActions';
import { initialState, orderReducer } from '../../reducers/orderReducer';
import Preloader from '../Preloader';
import style from './orders.module.css';


console.log(style);

function List() {

    const [state, dispatch] = useReducer(orderReducer, initialState);

    const getStatus = (status) => {
        switch (status) {
            case 'queued':
                return 'new badge orange';
            case 'in progress':
                return 'new badge blue';
            case 'produced' || 'validate':
                return 'new badge green';
            case 'validate':
                return 'new badge green';
            case 'in validation':
                return 'new badge purple';
            case 'stocked':
                return 'new badge grey';
            default:
                return 'new badge red';
        }
    }

    const handleSendToQuality = (order) => {
        sendToQuality(dispatch, {
            order: order._id
        }).then((data) => {
            M.toast({
                html: `Order: ${data._id} sent to Quality.`
            });
            getOrders(dispatch);
        })
    }

    const getOrderItem = (order) => {
        return (
            <li key={order._id}>
                <div className="collapsible-header">
                    {order.product.name}  {(new Date(order.date)).toLocaleDateString()}
                    <span className={getStatus(order.status)} data-badge-caption={order.status}/>
                </div>
                <div className="collapsible-body">
                    <ul className="collection">
                        <li className="collection-item">
                            Quantity: <span className="new badge grey" data-badge-caption="">{order.produced}/{order.quantity}</span>
                        </li>
                        {order.engineAssigned &&
                            <li className="collection-item">
                                Engine Assigned: <b>{order.engineAssigned && order.engineAssigned.name}</b>
                            </li>
                        }
                        {order.status === 'produced' &&
                            <li>
                                <input onClick={() => handleSendToQuality(order)} className="btn purple" type="button" value="Send to Quality"/>
                                <input onClick={() => handleSendToQuality(order)} className="btn grey" type="button" value="Send to Stock"/>
                            </li>
                        }
                    </ul>
                </div>
            </li>
        )
    }

    useEffect(() => {
        getOrders(dispatch)
    }, [])

    useEffect(() => {
        M.AutoInit();
    }, [state.orders])

    return (
        state.loading ? (
            <div style={{ marginTop: 48 }} className="container center">
                <Preloader/>
            </div>
        ) : (
            Boolean(state.orders.length) ? (
                <div className="container">
                    {['queued', 'in progress', 'produced', 'in validation', 'validate', 'stocked'].map(s => {
                        let orders = state.orders.sort((a, b) => (new Date(a.date) - (new Date(b.date)))).filter(item => item.status === s);
                        return (
                            <ul key={s} className={`${style.collapsibleMain} collapsible`}>
                                <li>
                                    <div className={`${style.collapsibleHead} collapsible-header`}>
                                        {s[0].toUpperCase() + s.slice(1)}
                                        <span className={getStatus(s)} data-badge-caption="">{orders.length}</span>
                                    </div>
                                    <div style={{ borderBottom: 0 }} className="collapsible-body">
                                        <ul className="collapsible popout">
                                            {orders.length 
                                                ? orders.map((item) => {
                                                    return getOrderItem(item);
                                                })
                                                : <p>No orders Found</p>
                                            }
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        )
                    })}
                </div>
            ) : (
                <div className="container">
                    <div className="card-panel hoverable">
                        <p>No raw matter found</p>
                    </div>
                </div>
            )
        )
    )
}


export default List;