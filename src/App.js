import React, { useEffect } from 'react';
import jwt_decode from 'jwt-decode';

import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { useAuthDispatch, useAuthState } from './contexts/authContext.js';
import { logout, setAuthToken } from './actions/authActions.js';

import Home from './components/Home.js';
import Login from './components/Login/index.js';
import Dashboard from './components/Dashboard/index.js';


function App() {

	const dispatch = useAuthDispatch();
	const userDetails = useAuthState();

	(function() {
		let token = userDetails.token;
		setAuthToken(token);
   	})();

	useEffect(() => {
		if (Boolean(userDetails.token)) {
			const decoded = jwt_decode(userDetails.token);
			const currentTime = Date.now() / 1000;
			if (decoded.exp < currentTime) {
				logout(dispatch, 'Session expired, please relogin')
			}
		}
	}, [userDetails, dispatch])

	return (
		<Router>
			<Switch>
				<Route exact path='/'>
					<Home />
				</Route>
				<Route exact path='/login'>
					<Login />
				</Route>
				{Boolean(userDetails.token) && 
					<Route path='/app'>
						<Dashboard />
					</Route>
				}
				<Route path='/*'>
					{Boolean(userDetails.token)
						? <Redirect to={{ pathname: '/app' }}/>
						: <Redirect to={{ pathname: '/login' }}/>
					}
				</Route>
			</Switch>
		</Router>
	);
}

export default App;