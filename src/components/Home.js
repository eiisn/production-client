import React from 'react';
import 'materialize-css';
import { Link } from 'react-router-dom';


function Home() {

    let deferredPrompt;

    window.addEventListener('beforeinstallprompt', (e) => {
        e.preventDefault();
        deferredPrompt = e;
    });

    window.addEventListener('appinstalled', () => {
        deferredPrompt = null;
    });

    const installPWA = async () => {
        deferredPrompt.prompt();
        const { outcome } = await deferredPrompt.userChoice;
        console.log(outcome)
        deferredPrompt = null;
    }

    return (
        <div className="">
            <nav className="grey darken-3">
                <div className="nav-wrapper">
                    <ul className="right">
                        <li><Link to='/login' className="btn white black-text">Log In</Link></li>
                    </ul>
                </div>
            </nav>
            <div style={{ paddingTop: 48 }} className="row center">
                <h1 style={{ fontSize: 48 }} className="">Welcome on Production Line</h1>
            </div>
            <div className="row center">
                {!window.matchMedia('(display-mode: standalone)').matches &&
                    <div onClick={installPWA} className="btn waves-effect waves-light blue lighten-3">Install App</div>
                }
            </div>
        </div>
    )
}


export default Home;