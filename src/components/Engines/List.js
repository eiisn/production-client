import React, { useEffect, useReducer } from 'react'
import { getEnginesByLines } from '../../actions/engineActions';
import { SocketProvider } from '../../contexts/socketContext';
import { initialStateLine, lineReducer } from '../../reducers/engineReducer';
import { Link } from 'react-router-dom';
import Engine from './Engine';


function List() {

    const [stateLine, dispatchLine] = useReducer(lineReducer, initialStateLine)

    useEffect(() => {
        getEnginesByLines(dispatchLine);
    }, [])

    return (
        <SocketProvider>
            <div className="container">
                <div style={{ marginTop: 48 }} className="row">
                    {stateLine.lines.map(line => {
                        return (
                            <div className="col s12" key={line._id}>
                                <ul className="collapsible">
                                    <li>
                                        <div className="collapsible-header">{line.name}</div>
                                        <div className="collapsible-body">
                                            <ul className="collection">
                                                <li className="collection-item">Engines: <b>{line.engines.length}</b></li>
                                                <li className="collection-item">Usage: <b>{line.usage}</b></li>
                                                <li className="collection-item">Description: <b>{line.description}</b></li>
                                            </ul>
                                            <Link to={`/app/engines/line/update/${line._id}`} className="btn blue waves-effect waves-light">Update</Link>
                                        </div>
                                    </li>
                                </ul>
                                <ul className="collapsible popout">
                                    {line.engines.sort((a, b) => { return a.position - b.position }).map(engine => {
                                        return <Engine key={engine._id} engine={engine} />
                                    })}
                                </ul>
                            </div>
                        )
                    })}
                </div>
            </div>
        </SocketProvider>
    )
}

export default List;