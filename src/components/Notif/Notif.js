import React from 'react';


function Notif (props) {

    return (
        <div className={`card-panel ${props.type}`}>
            <h5>{props.title}</h5>
            {props.children}
        </div>
    )
}

export default Notif;