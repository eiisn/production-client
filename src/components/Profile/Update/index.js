import React, { useEffect, useReducer, useState } from 'react';
import { useParams } from 'react-router';

import { getUser } from '../../../actions/userActions';
import { useAuthState } from '../../../contexts/authContext';
import { initialState, userReducer } from '../../../reducers/userReducer';
import { ADMIN, SUPERVISOR } from '../../../config/config.default';
import Preloader from '../../Preloader';
import Form from './Form';
import { RESET } from '../../../actions/types';


function Update() {

    const { username } = useParams();
    const userDetails = useAuthState();
    
    const [state, dispatch] = useReducer(userReducer, initialState);
    const [form, setForm] = useState(<Preloader/>)

    useEffect(() => {

        dispatch({ type: RESET })

        const getDisableChangeRole = (user) => {
            switch (userDetails.user.role) {
                case ADMIN:
                    if (user.role === ADMIN) {
                        return true;
                    }
                    return false;
                case SUPERVISOR:
                    if ([SUPERVISOR, ADMIN].includes(user.role)) {
                        return true;
                    }
                    return false;
                default:
                    return true;
            }
        }

        if (Boolean(username)) {
            getUser(dispatch, { username: username }).then((user) => {
                setForm(<Form user={user} changeRole={getDisableChangeRole(user)}/>)
            });
        } else {
            setForm(<Form user={userDetails.user} changeRole={getDisableChangeRole(userDetails.user)}/>)
        }

    }, [userDetails, username])

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                <h5>{state.user.username}</h5>
                {form}
            </div>
        </div>
    )
}

export default Update;