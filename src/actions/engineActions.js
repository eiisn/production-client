import axios from "axios";
import { ROOT_URL } from "../config/config.default";
import { 
    GET_ENGINES_ERROR, GET_ENGINES_SUCCESS, REQUEST_ENGINES, 
    REQUEST_LINES, GET_LINES_SUCCESS, GET_LINES_ERROR, 
    ADD_ENGINE_SUCCESS, ADD_ENGINE_ERROR, ADD_LINE_ERROR, 
    ADD_LINE_SUCCESS, UPDATE_ENGINE_SUCCESS, UPDATE_ENGINE_ERROR,
    GET_ENGINE_SUCCESS, GET_ENGINE_ERROR, REQUEST_LINE, GET_LINE_SUCCESS, GET_LINE_ERROR, UPDATE_LINE_SUCCESS, UPDATE_LINE_ERROR
} from "./types";


export async function getEngine(dispatch, engineId) {
    
    dispatch({ type: REQUEST_ENGINES })

    try {
        let res = await axios.get(`${ROOT_URL}/api/engine/e/${engineId}`)
        dispatch({ type: GET_ENGINE_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_ENGINE_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_ENGINE_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function getEngines(dispatch) {
    
    dispatch({ type: REQUEST_ENGINES })

    try {
        let res = await axios.get(`${ROOT_URL}/api/engine/list`)
        dispatch({ type: GET_ENGINES_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_ENGINES_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_ENGINES_ERROR, error: err.toString() });
        return err.toString();
    }
}

export async function addEngine(dispatch, payload) {

    dispatch({ type: REQUEST_ENGINES });

    console.log(payload);

    try {
        let res = await axios.post(`${ROOT_URL}/api/engine/create`, payload)
        dispatch({ type: ADD_ENGINE_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: ADD_ENGINE_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: ADD_ENGINE_ERROR, error: err.toString() });
        return err.toString();
    }
}

export async function updateEngine(dispatch, payload) {

    dispatch({ type: REQUEST_ENGINES });

    console.log(payload);

    try {
        let res = await axios.post(`${ROOT_URL}/api/engine/update`, payload);
        dispatch({ type: UPDATE_ENGINE_SUCCESS, payload: res.data });
        return res.data;
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: UPDATE_ENGINE_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: UPDATE_ENGINE_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function getEnginesByLines(dispatch) {

    dispatch({ type: REQUEST_LINES });

    try {
        let res = await axios.get(`${ROOT_URL}/api/line/list/engines`)
        dispatch({ type: GET_LINES_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_LINES_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_LINES_ERROR, error: err.toString() });
        return err.toString();
    }
}


export async function getLines(dispatch) {

    dispatch({ type: REQUEST_LINES });

    try {
        let res = await axios.get(`${ROOT_URL}/api/line/list`)
        dispatch({ type: GET_LINES_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_LINES_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_LINES_ERROR, error: err.toString() });
        return err.toString();
    }
}

export async function getLine(dispatch, lineId) {

    dispatch({ type: REQUEST_LINE });

    try {
        let res = await axios.get(`${ROOT_URL}/api/line/l/${lineId}`)
        dispatch({ type: GET_LINE_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: GET_LINE_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: GET_LINE_ERROR, error: err.toString() });
        return err.toString();
    }
}

export async function updateLine(dispatch, payload) {

    dispatch({ type: REQUEST_LINE });

    console.log(payload);

    try {
        let res = await axios.post(`${ROOT_URL}/api/line/update`, payload);
        dispatch({ type: UPDATE_LINE_SUCCESS, payload: res.data });
        return res.data;
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: UPDATE_LINE_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: UPDATE_LINE_ERROR, error: err.toString() });
        return err.toString();
    }
}

export async function addLine(dispatch, payload) {

    dispatch({ type: REQUEST_LINES });

    try {
        let res = await axios.post(`${ROOT_URL}/api/line/create`, payload)
        dispatch({ type: ADD_LINE_SUCCESS, payload: res.data });
        return res.data
    } catch (err) {
        if (err.response) {
            if (err.response.data) { 
                dispatch({ type: ADD_LINE_ERROR, error: err.response.data.message });
                return err;
            }
        }
        dispatch({ type: ADD_LINE_ERROR, error: err.toString() });
        return err.toString();
    }
}
