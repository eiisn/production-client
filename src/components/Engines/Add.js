import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';
import { addEngine, getLines } from '../../actions/engineActions';
import { 
    initialStateEngine, engineReducer, 
    lineReducer, initialStateLine 
} from '../../reducers/engineReducer';
import Success from '../Notif/Success';
import Error from '../Notif/Error';

import Preloader from '../Preloader';


function Add() {

    const [stateEngine, dispatchEngine] = useReducer(engineReducer, initialStateEngine);
    const [stateLine, dispatchLine] = useReducer(lineReducer, initialStateLine)
    
    const [ip, setIp] = useState(undefined)
    const [port, setPort] = useState(undefined)
    const [name, setName] = useState(undefined)
    const [description, setDescription] = useState(undefined)
    const [line, setLine] = useState(undefined)
    const [position, setPosition] = useState(undefined)
    
    useEffect(() => {
        getLines(dispatchLine)
    }, [])

    useEffect(() => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
    }, [stateLine.lines])

    const handleSubmit = async (e) => {
        addEngine(dispatchEngine, {
            ip: ip,
            port: port,
            name: name,
            description: description,
            line: line,
            position: position
        })
    }

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {stateEngine.loading || stateLine.loading ? (
                    <Preloader/>
                ) :
                <div className="col s12">
                    <form>
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title">Create New Engine</span>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id="engine-ip" type="text" onChange={(e) => { setIp(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-ip">Ip</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input id="engine-port" type="text" onChange={(e) => { setPort(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-port">Port</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input id="engine-name" type="text" onChange={(e) => { setName(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-name">Name</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input id="engine-description" type="text" onChange={(e) => { setDescription(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-description">Description</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <select defaultValue="" onChange={(e) => { setLine(e.target.value) }}>
                                            <option value="" disabled>Select Production Line</option>
                                            {stateLine.lines.map(l => {
                                                return <option key={l._id} value={l._id}>{l.name}</option>
                                            })}
                                        </select>
                                        <label>Line</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input id="engine-position" type="number" onChange={(e) => { setPosition(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-position">Position</label>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action">
                                <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Create"/>
                            </div>
                        </div>
                    </form>
                </div>   
                }
            </div>
            {stateEngine.errorMessage && 
                <div className="row">
                    <Error title="Engine creation error">
                        <p>{stateEngine.errorMessage}</p>
                    </Error>
                </div>
            }
            {stateEngine.newEngine.name &&
                <div className="row">
                    <Success title="Engine created">
                        <p>Engine: {stateEngine.newEngine.name}</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Add;