import React, { useEffect, useReducer, useState } from 'react';
import M from 'materialize-css';
import { getLine, updateLine } from '../../../actions/engineActions';
import { lineReducer, initialStateLine } from '../../../reducers/engineReducer';
import Success from '../../Notif/Success';
import Error from '../../Notif/Error';

import Preloader from '../../Preloader';
import { useParams } from 'react-router';


function Update() {

    const { lineId } = useParams();

    const [stateLine, dispatchLine] = useReducer(lineReducer, initialStateLine)
    
    const [newName, setNewName] = useState('');
    const [newDescription, setNewDescription] = useState('');
    const [newUsage, setNewUsage] = useState('');

    const updateForm = () => {
        let elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {
            dropdownOptions: {
                coverTrigger: false
            }
        })
        setTimeout(M.updateTextFields, 100);
    }
    
    useEffect(() => {
        getLine(dispatchLine, lineId);
    }, [lineId])

    useEffect(() => {
        console.log(stateLine)
        setNewName(stateLine.line.name);
        setNewDescription(stateLine.line.description);
        setNewUsage(stateLine.line.usage);
        setTimeout(updateForm, 100);
    }, [stateLine.line, stateLine.updated])

    const handleSubmit = async (e) => {
        updateLine(dispatchLine, {
            _id: lineId,
            newName: newName,
            newDescription: newDescription,
            newUsage: newUsage
        })
    }

    return (
        <div className="container">
            <div style={{ marginTop: 48 }} className="row">
                {stateLine.loading ? (
                    <Preloader/>
                ) :
                <div className="col s12">
                    <form>
                        <div className="card">
                            <div className="card-content">
                                <span className="card-title">Update Line</span>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input value={newName} id="line-name" type="text" onChange={(e) => { setNewName(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-ip">Name</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input value={newDescription} id="line-description" type="text" onChange={(e) => { setNewDescription(e.target.value) }} className="validate"/>
                                        <label htmlFor="engine-description">Description</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <select defaultValue="production" value={newUsage} onChange={(e) => { setNewUsage(e.target.value) }}>
                                            <option key="production" value="production">Production</option>
                                            <option key="stock" value="stock">Stock</option>
                                        </select>
                                        <label>Line</label>
                                    </div>
                                </div>
                            </div>
                            <div className="card-action">
                                <input onClick={handleSubmit} type="submit" className="btn green waves-effect waves-light" value="Update"/>
                            </div>
                        </div>
                    </form>
                </div>   
                }
            </div>
            {stateLine.errorMessage && 
                <div className="row">
                    <Error title="Line update error">
                        <p>{stateLine.errorMessage}</p>
                    </Error>
                </div>
            }
            {stateLine.updated &&
                <div className="row">
                    <Success title="Line">
                        <p>Update succeed</p>
                    </Success>
                </div>
            }
        </div>
    )
}


export default Update;